import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/galery',
    name: 'galery',    
    component: () => import('../views/GaleryView.vue')
  },
  {
    path: '/contact',
    name: 'contact',    
    component: () => import('../views/ContactView.vue')
  },
  {
    path: '/stocks',
    name: 'stocks',    
    component: () => import('../views/StocksView.vue')
  },
  {
    path: '/stock',
    name: 'stock',    
    component: () => import('../views/StockView.vue')
  },
  {
    path: '/catalog',
    name: 'catalog',    
    component: () => import('../views/CatalogView.vue')
  },
  {
    path: '/catalog/:id',
    name: 'tovar',    
    component: () => import('../views/TovarView.vue')
  },
  {
    path: '/about',
    name: 'about',    
    component: () => import('../views/AboutView.vue')
  },   
  {
    path: '/payment',
    name: 'payment',    
    component: () => import('../views/PaymentView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
